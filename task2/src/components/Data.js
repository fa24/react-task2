import React from 'react'

const Data = (props) => {
    return (
        <>
       {console.log("hii")}
        <div className="row d-flex justify-content-center align-items-center mt-3 ">
        <div className="col-md-8 ">
          <div className="card">
            <div className="card-body">
              <div className="row d-inline-block">
                <p className="card-text" id="name">
                  {props.name}
                </p>
                <p className="card-text lead" id="contactNo">
                  {props.contact}
                </p>
              </div>
              <div className="display-6" id="email">
                {props.email}
              </div>
              <div className="display-6" id="address">
                {props.address}
              </div>
            </div>
          </div>
        </div>
      </div>
      </>
    )
}

export default Data
