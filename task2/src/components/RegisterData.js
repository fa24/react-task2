import React, { useState } from "react";
import Form from './Form';
import  FormSuccess  from './FormSuccess'
const RegisterData = () => {
  const [isSubmited, setIsSubmited] = useState(false);

  const submitForm = () => {
    setIsSubmited(true)
  };


  return (
      <div>
            {!isSubmited ?
                <Form onsubmitForm={submitForm} ></Form>:
                <FormSuccess></FormSuccess>}
                );
      </div>
  )
 
};

export default RegisterData;
