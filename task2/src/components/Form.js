import "../../node_modules/bootstrap/dist/css/bootstrap.min.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUser } from "@fortawesome/free-solid-svg-icons";
import React, { useState ,useEffect} from "react";

const Form = (props) => {
  //For setting the user form values
  const [values, setValues] = useState({
    name: "",
    contactNo: "",
    email: "",
    address: "",
  });

  const [isSubmitting,setIsSubmitting] =  useState(false)
  //For setting the error messages from  form values
  const [errors, setErrors] = useState({});
  // for submitting the form 



  const validateInfo = (values) => {
    let errors = {};
    if (!values.name.trim()) {
      errors.name = "Name Required";
    }
    if (!values.contactNo) {
      errors.contactNo = "Contact no Required";
    } else if (values.contactNo.length !== 10) {
      errors.contactNo = "Contact no is Invalid";
    }
    //Email
    if (!values.email) {
      errors.email = "Email Required";
    } else if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(values.email)
    ) {
      errors.email = "Email Address is Invalid";
    }
    if (!values.address) {
      errors.address = "Address Required";
    }
    else if(!(values.address.length > 10)){
      errors.address = "Address is too short ";
    }
    

    return errors;
  };


  const changeHandler = (e) => {
    setValues({
      ...values,
      [e.currentTarget.id]: e.currentTarget.value,
    });

  };
  const submitHandler = (e) => {
    e.preventDefault();
    console.log(values)
    setErrors(validateInfo(values)) 
    setIsSubmitting(true)
  };

  useEffect(
    () => {
      if (Object.keys(errors).length === 0 && isSubmitting) {
        props.onsubmitForm();
      }
    },
    [errors]
  );
  return (
    <form
      className="d-flex justify-content-center align-items-center mt-3 "
      id="form"
      onSubmit={submitHandler}
    >
      <div className="card col-md-4 border-success">
        <div className="card-header">
          <h1 className="display-6 text-center">
            <span className="me-2">
              <FontAwesomeIcon icon={faUser} size="lg" />
            </span>
            Login
          </h1>
        </div>
        <div className="card-body gy-2">
          <div className="col-md-12">
            <label htmlFor="name" className="form-label">
              Name
            </label>
            <input
              type="text"
              className="form-control"
              id="name"
              value={values.name}
              onChange={changeHandler}
              
            />
           {errors.name && <div className="text-danger"
            > {errors.name}</div>}
          </div>
          <div className="col-md-12">
            <label htmlFor="contactNo" className="form-label">
              Contact Number
            </label>
            <input
              type="number"
              className="form-control "
              id="contactNo"
              min="10"
              value={values.contactNo}
              onChange={changeHandler}
              required
            />
            {errors.contactNo && <div className="text-danger">{errors.contactNo}
             
            </div>}
          </div>
          <div className="col-md-12">
            <label htmlFor="email" className="form-label ">
              Email
            </label>
            <div className="input-group">
              <span
                className="input-group-text text-dark"
                id="inputGroupPrepend"
              >
                @
              </span>
              <input
                type="email"
                className="form-control "
                id="email"
                aria-describedby="inputGroupPrepend"
                value={values.email}
                onChange={changeHandler}
                required
              />
            </div>
             {errors.email && <div className="text-danger">{errors.email}</div>}
          </div>
          <div className="col-md-12">
            <label htmlFor="address" className="form-label ">
              Address
            </label>
            <div className="input-group">
              <textarea
                type="text-area"
                className="form-control "
                id="address"
                aria-describedby="inputGroupPrepend"
                value={values.address}
                onChange={changeHandler}
                required
              />
            </div>
               {errors.address && <div className="text-danger">{errors.address}</div>}
          </div>
        </div>
        <div className="card-footer text-muted">
          <div className="col-md-12 d-flex justify-content-center">
            <button className="btn btn-success" type="submit">
              Submit
            </button>
          </div>
        </div>
      </div>
    </form>
  );
};

export default Form;
