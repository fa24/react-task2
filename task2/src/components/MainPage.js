import React, { useState, useEffect } from "react";
import RegisterData from "./RegisterData";
import ShowData from "./ShowData";
import Topbuttons from "./Topbuttons";

const MainPage = () => {

  const [button, setButton] = useState(true);
  const [data, setdata] = useState("");

  useEffect(() => {
    getData();
  }, []);

  const getData = async () => {
    try {
      const result = await fetch(
        "https://dry-bayou-99944.herokuapp.com/profiles",
        {
          headers: {
            "Content-Type": "application/json",
            Accept: "application/json",
          },
        }
      );

      const res = await result.json();
      console.log(res)
      setdata(res)
    } catch {}
  };

  return (
    <>
     {console.log(button)}
      <Topbuttons setButtonState={setButton} />
      {button ? <RegisterData /> : <ShowData setData={data}/>}
    </>
  );
};

export default MainPage;
