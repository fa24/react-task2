import React, { useState } from "react";

const ShowData = (props) => {
  const [data, setdata] = useState(props.setData);

  return (
    <>
      {data &&
        data.length > 0 &&
        data.map((i, index) => {
          if (i.name && i.contact && i.email && i.address) {
            return (
              <div className="row d-flex justify-content-center align-items-center mt-3 ">
                <div className="col-md-8 ">
                  <div className="card">
                    <div className="card-body">
                      <div className="row d-inline-block">
                        <p className="card-text" id="name" key={i.name}>
                          {i.name}
                        </p>
                        <p
                          className="card-text lead"
                          id="contactNo"
                          key={i.contact}
                        >
                          {i.contact}
                        </p>
                      </div>
                      <div className="display-6" id="email" key={i.email}>
                        {i.email}
                      </div>
                      <div className="display-6" id="address" key={i.address}>
                        {i.address}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            );
          }
        })}
    </>
  );
};

export default ShowData;
