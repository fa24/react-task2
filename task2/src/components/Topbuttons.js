import React from "react";

const Topbuttons = (props) => {
  const buttonHandle = (e) => {
      
    e.currentTarget.id == "showData"
      ? props.setButtonState(false)
      : props.setButtonState(true);
  };

  return (
    <div className="row mt-5">
      <div className="d-flex justify-content-center">
        <button
          type="button"
          className="btn btn-success btn-sm me-2"
          id="addData"
          onClick={buttonHandle}
        >
          Add Data
        </button>
        <button
          type="button"
          className="btn btn-primary btn-sm"
          id="showData"
          onClick={buttonHandle}
        >
          Show Data
        </button>
      </div>
    </div>
  );
};

export default Topbuttons;
